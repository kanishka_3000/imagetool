#ifndef GRAPHICSRESIZETRACKER_H
#define GRAPHICSRESIZETRACKER_H

#include <QGraphicsPixmapItem>
class GraphiItemCtrl;
class GraphicsResizeTracker : public QGraphicsPixmapItem
{

public:
    explicit GraphicsResizeTracker(QPointF oCenter,GraphiItemCtrl* pParent);

protected:
    virtual void 	mouseReleaseEvent(QGraphicsSceneMouseEvent * pEvent);
    GraphiItemCtrl* p_Parent;
};


#endif // GRAPHICSRESIZETRACKER_H
