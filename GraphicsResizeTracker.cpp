#include "GraphicsResizeTracker.h"
#include <QGraphicsSceneEvent>
#include <GraphiItemCtrl.h>
GraphicsResizeTracker::GraphicsResizeTracker(QPointF oCenter,GraphiItemCtrl* pParent) :
    QGraphicsPixmapItem(QPixmap(":/ImageTool/Resources/537986.png")),p_Parent(pParent)
{
    setX(oCenter.x() - 10 );
    setY(oCenter.y() - 10 );
}



void GraphicsResizeTracker::mouseReleaseEvent(QGraphicsSceneMouseEvent *pEvent)
{
    QPointF oPoint = pEvent->scenePos();
    p_Parent->OnRelocateChecked(oPoint);
}

