#ifndef GRAPHICSIMAGELIST_H
#define GRAPHICSIMAGELIST_H

#include <QWidget>
#include <QListWidget>
#include <QModelIndexList>
#include <GraphicImage.h>
#include <QSet>
#include <QTimer>
#include "ui_GraphicsImageList.h"
typedef QMap<int, QListWidgetItem*> WidgetMap;
class GraphicsImageList : public QListWidget, public Ui::GraphicsImageList
{
	Q_OBJECT

public:
	GraphicsImageList(QWidget *parent = 0);
	~GraphicsImageList();
	void AddItem(GraphicImage* pImage, int iIndex);
	void RemoveItem(int iIndex);
	void RemoveAll();

	void dropEvent ( QDropEvent * pEvent );
	void ResetAndLock(bool bLock);
	QSet<GraphicImage*> GetSelectedImages();
	QMap<QListWidgetItem*,GraphicImage*> map_Images;
	WidgetMap map_Indices;
	QSet<QListWidgetItem*> set_SelectedItems;
	
protected:
	QListWidgetItem* p_LastImage;
	
signals:
	void NotifySelectedLastIndex(GraphicImage* pLastImage);

public slots:
	void OnSelectionChanged();
	void OnItemClicked(QListWidgetItem* pItem);
	
};

#endif // GRAPHICSIMAGELIST_H
