#ifndef __GraphicImage__
#define __GraphicImage__

#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneWheelEvent>
#include <QSlider>
class GraphiItemCtrl;

class GraphicImage : public QObject,
	public QGraphicsPixmapItem
{
	
	Q_OBJECT
	

public:
    GraphicImage(QPixmap oImage,GraphiItemCtrl* pParent);
    virtual void	mousePressEvent ( QGraphicsSceneMouseEvent * pEvent );
    virtual void	mouseReleaseEvent ( QGraphicsSceneMouseEvent * pEvent );
    virtual void	hoverMoveEvent(QGraphicsSceneHoverEvent * event);
    virtual void	mouseMoveEvent ( QGraphicsSceneMouseEvent * event );
	//Overridden to support resize event initialization
    virtual void 	mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event);

    void SetSlier(QSlider* pSlier);
    virtual ~GraphicImage(void);

    QString s_Name;
private:
    GraphiItemCtrl* p_Parent;
    QSlider* p_Slier;
    public slots:
        void OnOpacityChanged(int iOpacity);
};

#endif // __GraphicImage__

