#ifndef GRAPHICHOLDERCTRL_H
#define GRAPHICHOLDERCTRL_H

#include <QWidget>
#include "ui_GraphicHolderCtrl.h"

class GraphicHolderCtrl : public QWidget, public Ui::GraphicHolderCtrl
{
	Q_OBJECT

public:
	GraphicHolderCtrl(QWidget *parent = 0);
	~GraphicHolderCtrl();

private:
	QString s_CurrentFileName;
public slots:
	void OpenFileDialog();
	void OnOk();
	void OnRemove();
	void OnClearAll();
	void OnClearDrawings();

	///>> Slots for demo purpose
	void OnDemoLineAdded(QLineF oLine);
	void OnDemoRectAdded(QRectF oRect);
	void OnDemoElipseAdded(QRectF oRect);
    void OnDemoCircleAdded(QRectF oRect);
	///>>>
};

#endif // GRAPHICHOLDERCTRL_H
