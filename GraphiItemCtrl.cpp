#include "GraphiItemCtrl.h"
#include <QMessageBox>
#include <QString>
#include <QDebug>
#include <QColorDialog>
#include <QShortcut>
#include <QGraphicsItemGroup>
#include <Util.h>
#include <cmath>
#include <GraphicsResizeTracker.h>
void GetLeftAndWidth(float x0, float y0, float x2, float y2, float& fTopLeftX,
                     float& fTopLeftY, float& fWidth, float& fHeight)
{

    float fradius, fyDistance, fxDistance;
    fyDistance = y2- y0;
    fxDistance = x2- x0;

    fradius = sqrt( (fyDistance * fyDistance) + (fxDistance * fxDistance));
    fTopLeftY = y0 - fradius;
    fTopLeftX = x0 - fradius;

    fWidth = 2 * fradius;
    fHeight = 2 * fradius;
}
GraphiItemCtrl::GraphiItemCtrl(QWidget *parent)
    :QWidget(parent),o_Text("ts",&o_Ruler),b_InAutoMode(false)
{
    setupUi(this);
    d_Scale = 1;
    i_NextZIndex = 1;
    p_SlierLayout  = new QHBoxLayout(this);
    b_MouseClicked = false;
    p_LastItem = NULL;
    i_MaxHeight = 0;
    i_MaxWidth = 0;
    i_ActivityMode = ACTIVTITY_MODE_SELECT;
    p_SelectedItem = NULL;
    p_SelectionDot = NULL;
    b_ItemRedrawn = false;
    b_SingleDraw = false;
    i_MaxIndex = 0;
    QPen oPen;
    oPen.setStyle(Qt::DashLine);
    o_Ruler.setPen(oPen);
    o_Ruler.setZValue(Z_INDEX_DRAWN_ITEMS);

    o_Line.setPen(oPen);
    o_Line.setZValue(Z_INDEX_DRAWN_ITEMS);
    o_Rect.setPen(oPen);
    o_Rect.setZValue(Z_INDEX_DRAWN_ITEMS);
    o_Elipse.setPen(oPen);
    o_Elipse.setZValue(Z_INDEX_DRAWN_ITEMS);
    //Loading image >>>

    //AddImage(QPixmap(":/ImageTool/Resources/Penguins.jpg"),i_NextZIndex++);
    ////Loading image <<<

    p_GraphicView->setScene(&o_ItemScene);


    o_MainActivity.addButton(rdo_Draw);
    o_MainActivity.addButton(rdo_Erase);
    o_MainActivity.addButton(rdo_Select);
    o_MainActivity.addButton(rdo_Point);
    o_MainActivity.addButton(rdo_Ruler);

    o_DrawType.addButton(btn_Line);
    o_DrawType.addButton(btn_Rectangle);
    o_DrawType.addButton(btn_Elipse);
    o_DrawType.addButton(btn_Cross);
    o_DrawType.addButton(btn_Dot);
    o_DrawType.addButton(btn_Circle);

    o_Zooming.addButton(btn_ZoomIn);
    o_Zooming.addButton(btn_ZoomOut);


    i_CurrentType = 6;

    lbl_SelColor->setBackgroundRole(QPalette::Background);
    lbl_SelColor->setAutoFillBackground(true);

    QPalette oPlaet;
    oPlaet.setColor(QPalette::Background, Qt::black);
    lbl_SelColor->setPalette(oPlaet);
    i_CurrentWidth = 3;
    e_CurrentColor = Qt::black;
    o_CurrentPen.setColor(e_CurrentColor);
    o_CurrentPen.setWidth(i_CurrentWidth);
    p_ColorDialog = new QColorDialog(this);

    p_GraphicView->setInteractive(false);
    p_GraphicView->setDragMode(QGraphicsView::ScrollHandDrag);

    frm_ClrSelection->setEnabled(false);
    frm_ItemSelection->setEnabled(false);
    frm_SizeSelection->setEnabled(false);

    connect(&o_MainActivity, SIGNAL(buttonClicked ( QAbstractButton *  )),
        this, SLOT(OnButtonSelected( QAbstractButton *  )));
    connect(&o_DrawType, SIGNAL(buttonClicked(QAbstractButton*)),
        this, SLOT(OnDrawTypeChanged(QAbstractButton *)));
    connect(p_ColorDialog, SIGNAL(colorSelected ( const QColor   )),
        this, SLOT(ColorSelected( QColor  )));
    connect(&o_Zooming, SIGNAL(buttonClicked ( QAbstractButton *  )),
        this, SLOT(OnZoomChanged(QAbstractButton * )));
    connect(p_ItemList, SIGNAL(NotifySelectedLastIndex(GraphicImage* )),
        this, SLOT(OnResetLastImage(GraphicImage* )));
    connect(&o_Timer, SIGNAL(timeout ()),this, SLOT(OnIterateTimer()));

}

GraphiItemCtrl::~GraphiItemCtrl()
{


}



void GraphiItemCtrl::paintEvent( QPaintEvent * event )
{


    QWidget::paintEvent(event);


}

void GraphiItemCtrl::DeselectResize()
{
    if(!p_SelectedItem)
        return;

    if(p_SelectionDot)
    {
        o_ItemScene.removeItem(p_SelectionDot);
        delete p_SelectionDot;
    }

    p_SelectedItem = NULL;
    p_SelectionDot = NULL;
}

void GraphiItemCtrl::OnMouseClickEvent(QPointF oPoint )
{


    if(i_ActivityMode == ACTIVITY_MODE_POINT)
    {
        DeselectResize();
        QGraphicsItem* pItem = o_ItemScene.itemAt(oPoint,QTransform());
        if(pItem)
        {
            if(set_Images.contains(pItem))
            {
                return;
            }
            b_ItemRedrawn = true;
            p_SelectedItem = pItem;

            o_PointA = oPoint;
        }

        return;
    }


    o_PointA = oPoint;
    if(i_ActivityMode == ACTIVITY_MODE_RULER)
    {
        if(b_MouseClicked == false)
        {
            o_Ruler.setLine(o_PointA.x(),o_PointA.y(),o_PointA.x(),o_PointA.y() );
            o_ItemScene.addItem(&o_Ruler);

            b_MouseClicked = true;
        }
        else
        {
            o_ItemScene.removeItem(&o_Ruler);
            b_MouseClicked = false;
        }
    }

    if(i_ActivityMode == ACTIVTITY_MODE_DRAW)
    {
        setCursor(Qt::CrossCursor);

        switch(i_CurrentType)
        {
        case ITM_TYPE_LINE:
            {
                o_Line.setLine(o_PointA.x(),o_PointA.y(),o_PointA.x(),o_PointA.y() );
                o_ItemScene.addItem(&o_Line);
                break;
            }
        case ITM_TYPE_RECTANGLE:
            {
                o_Rect.setRect(o_PointA.x(),o_PointA.y(),0,0);
                o_ItemScene.addItem(&o_Rect);
                break;
            }
        case ITM_TYPE_ELIPSE:
            {
                o_Elipse.setRect(o_PointA.x(),o_PointA.y(),0,0 );
                o_ItemScene.addItem(&o_Elipse);
                break;
            }
        case ITM_TYPE_CIRCLE:
        {                o_Elipse.setRect(o_PointA.x(),o_PointA.y(),0,0 );
            o_ItemScene.addItem(&o_Elipse);
            break;
        }
        }

    }
}

void GraphiItemCtrl::OnMouseReleaseEvent( QPointF oPoint  )
{

    if(i_ActivityMode == ACTIVITY_MODE_POINT)
    {
        if(b_ItemRedrawn)
        {
            b_ItemRedrawn = false;
            //if(o_SelectedRect != oCurrantRect)
            {
                double dWidth = oPoint.x() - o_PointA.x();
                double dHeight = oPoint.y() - o_PointA.y();
                int iIndex = p_SelectedItem->data(DRAWITM_INDEX).toInt();
                QRectF oPrevRect = p_SelectedItem->sceneBoundingRect();
                p_SelectedItem->moveBy(dWidth, dHeight);
                QRectF oCurrentRect = p_SelectedItem->sceneBoundingRect();
                if(oPrevRect != oCurrentRect)
                {

                    NotifyItemMoved(oPrevRect, oCurrentRect);
                    qDebug() << "Notify Item Moved" << oPrevRect << "==" << oCurrentRect;
                }
                /////////
                int iType = p_SelectedItem->type();
                if(iType == ITM_TYPE_ELIPSE && p_SelectedItem->data(IS_CICLE).toBool())
                {
                    iType = ITM_TYPE_CIRCLE;
                }
                switch(iType)
                {
                    case ITM_TYPE_RECTANGLE:
                    {

                    NotifyRectMoved(oPrevRect,oCurrentRect);
                    NotifyRectMoved(iIndex, oPrevRect,oCurrentRect);
                     qDebug() << "Notify Rect Moved" << iIndex<< " : "<< oPrevRect << "==" << oCurrentRect;
                        break;
                    }
                    case ITM_TYPE_ELIPSE:
                    {

                    NotifyElipseMoved(oPrevRect,oCurrentRect);
                    NotifyElipseMoved(iIndex, oPrevRect,oCurrentRect);
                     qDebug() << "Notify Elipse Moved"<< iIndex<< " : " << oPrevRect << "==" << oCurrentRect;
                        break;
                    }
                    case ITM_TYPE_LINE:
                    {
                    QLineF oPrevLine(oPrevRect.topLeft(),oPrevRect.bottomRight());
                    QLineF oCurLine(oCurrentRect.topLeft(),oCurrentRect.bottomRight());
                    NotifyLineMoved(oPrevLine,oCurLine);
                    NotifyLineMoved(iIndex,oPrevLine,oCurLine);
                     qDebug() << "Notify Line Moved"<< iIndex<< " : " << oPrevLine << "==" << oCurLine;
                        break;
                    }
                case ITM_TYPE_CIRCLE:
                {
                    NotifyCircleMoved(oPrevRect,oCurrentRect);
                    NotifyCircleMoved(iIndex, oPrevRect,oCurrentRect);
                    qDebug() << "Notify Circle Moved"<< iIndex<< " : " << oPrevRect << "==" << oCurrentRect;
                    break;
                }
                }
                ////////

            }


            delete p_SelectionDot;
            p_SelectionDot = NULL;

        }
        return;
    }

    o_PointB =  oPoint;

    setCursor(Qt::ArrowCursor);
    if(o_MainActivity.checkedButton() == rdo_Draw)
    {
        OnDrawEvent();
    }
    else if(o_MainActivity.checkedButton() == rdo_Erase)
    {
        OnRemoveEvent();

    }

    if(i_ActivityMode == ACTIVTITY_MODE_DRAW)
    {
        switch(i_CurrentType)
        {
        case ITM_TYPE_LINE:
            {
                o_ItemScene.removeItem(&o_Line);

                break;
            }
        case ITM_TYPE_RECTANGLE:
            {
                o_ItemScene.removeItem(&o_Rect);

                break;
            }
        case ITM_TYPE_ELIPSE:
        case ITM_TYPE_CIRCLE:
            {
                o_ItemScene.removeItem(&o_Elipse);

                break;
            }
        }
        rdo_Point->click();
    }

}

void GraphiItemCtrl::OnDrawItemChanged( QString sItemType )
{

    s_CurrentItem = sItemType;

}

void GraphiItemCtrl::OnDrawEvent()
{

    switch(i_CurrentType)
    {
    case ITM_TYPE_LINE:
        {
            ClearAllSelectionCheck();
            QGraphicsLineItem* pLine = o_ItemScene.addLine(o_PointA.x(),o_PointA.y(),
                o_PointB.x(), o_PointB.y(),o_CurrentPen);
            pLine->setZValue(Z_INDEX_DRAWN_ITEMS);
            QLineF oLine = pLine->line();
            int iAutoIndex = i_MaxIndex + 1;
            ReplaceFromIndex(iAutoIndex , pLine);
            emit NotifyLineAdded(oLine);
            emit NotifyLineAdded(iAutoIndex, oLine);
            break;
        }
    case ITM_TYPE_RECTANGLE:
        {
            ClearAllSelectionCheck();
            double dWidth = o_PointB.x() - o_PointA.x();
            double dHeight = o_PointB.y() - o_PointA.y();
            QPointF oPointA = o_PointA;
            AdjustSizes(dHeight, oPointA, dWidth);

            QGraphicsRectItem* pRect = o_ItemScene.addRect(oPointA.x(),oPointA.y(),
                dWidth ,dHeight,o_CurrentPen);
            pRect->setZValue(Z_INDEX_DRAWN_ITEMS);

            QRectF oRect = pRect->rect();
            int iAutoIndex = i_MaxIndex + 1;
            ReplaceFromIndex(iAutoIndex , pRect);
            emit NotifyRectAdded(oRect);
            emit NotifyRectAdded(iAutoIndex, oRect);
            break;
        }
    case ITM_TYPE_ELIPSE:
        {
            ClearAllSelectionCheck();
            double dWidth = o_PointB.x() - o_PointA.x();
            double dHeight = o_PointB.y() - o_PointA.y();

            QPointF oPointA = o_PointA;
            AdjustSizes(dHeight, oPointA, dWidth);

            QGraphicsEllipseItem* pElipse =	o_ItemScene.addEllipse(oPointA.x(),oPointA.y(),
                dWidth ,dHeight,o_CurrentPen);
            pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);
            QRectF oRect = pElipse->rect();

            int iAutoIndex = i_MaxIndex + 1;
            ReplaceFromIndex(iAutoIndex , pElipse);

            emit NotifyElipseAdded(oRect);
            emit NotifyElipseAdded(iAutoIndex, oRect);
            break;
        }
    case ITM_TYPE_CIRCLE:
    {
         ClearAllSelectionCheck();
         float fTopLeftX, fTopLeftY, fWidth, fHeight;
         GetLeftAndWidth(o_PointA.x(), o_PointA.y(), o_PointB.x(), o_PointB.y(),fTopLeftX,
                         fTopLeftY, fWidth, fHeight);

         QPointF oPointA = o_PointA;

         QGraphicsEllipseItem* pElipse =	o_ItemScene.addEllipse(fTopLeftX, fTopLeftY,
             fWidth ,fHeight,o_CurrentPen);
         pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);
         pElipse->setData(IS_CICLE, true);
         QRectF oRect = pElipse->rect();
         int iAutoIndex = i_MaxIndex + 1;
         ReplaceFromIndex(iAutoIndex , pElipse);
         emit NotifyCircleAdded(oRect);
         emit NotifyCircleAdded(iAutoIndex, oRect);
         break;

    }
    case ITM_TYPE_CROSS:
        {
            AddCross(o_PointA);
            break;
        }
    case ITM_TYPE_DOT:
        {
            AddDot(o_PointA);
            break;
        }
    }
}

void GraphiItemCtrl::OnRemoveEvent()
{
    QGraphicsItem* pItem = o_ItemScene.itemAt(o_PointB,QTransform());
    int iIndex = pItem->data(DRAWITM_INDEX).toInt();
    map_DrawnItems.remove(iIndex);
    if(set_Images.contains(pItem))
        return;
    QGraphicsItemGroup* pGroup = pItem->group();
    if(pGroup)
        pItem = pGroup;

    o_ItemScene.removeItem(pItem);
}



void GraphiItemCtrl::OnGraphicsScale( bool bZoomIn )
{
    double dScaleFactor = 1.1;
    if(bZoomIn)
    {
        p_GraphicView->scale(dScaleFactor,dScaleFactor);

        d_Scale += 0.1;

    }
    else
    {
        p_GraphicView->scale(1.0/dScaleFactor,1.0/dScaleFactor);
        QRectF imageRect = rect();
        d_Scale -= 0.1;

    }
    spin_Zoom->setValue(d_Scale);

}

void GraphiItemCtrl::OnButtonSelected( QAbstractButton * button )
{
    DeselectResize();
    if(button == rdo_Select)
    {

        i_ActivityMode = ACTIVTITY_MODE_SELECT;
        p_GraphicView->setInteractive(false);
        p_GraphicView->setDragMode(QGraphicsView::ScrollHandDrag);
        frm_ClrSelection->setEnabled(false);
        frm_ItemSelection->setEnabled(false);
        frm_SizeSelection->setEnabled(false);
        lbl_Coordinates->setText("");

    }
    else if (button == rdo_Draw)
    {
        i_ActivityMode = ACTIVTITY_MODE_DRAW;
        p_GraphicView->setInteractive(true);
        p_GraphicView->setDragMode(QGraphicsView::NoDrag);

        frm_ClrSelection->setEnabled(true);
        frm_ItemSelection->setEnabled(true);
        frm_SizeSelection->setEnabled(true);

    }
    else if (button == rdo_Erase)
    {
        i_ActivityMode = ACTIVTITY_MODE_DRAW;
        p_GraphicView->setInteractive(true);
        p_GraphicView->setDragMode(QGraphicsView::NoDrag);

        frm_ClrSelection->setEnabled(false);
        frm_ItemSelection->setEnabled(false);
        frm_SizeSelection->setEnabled(false);
    }
    else if (button == rdo_Point)
    {
        i_ActivityMode = ACTIVITY_MODE_POINT;
        p_GraphicView->setInteractive(true);
        p_GraphicView->setDragMode(QGraphicsView::NoDrag);

        frm_ClrSelection->setEnabled(false);
        frm_ItemSelection->setEnabled(false);
        frm_SizeSelection->setEnabled(false);

    }else if (button == rdo_Ruler)
    {
        i_ActivityMode = ACTIVITY_MODE_RULER;
        p_GraphicView->setInteractive(true);
        p_GraphicView->setDragMode(QGraphicsView::NoDrag);

        frm_ClrSelection->setEnabled(false);
        frm_ItemSelection->setEnabled(false);
        frm_SizeSelection->setEnabled(false);
    }
    setCursor(Qt::ArrowCursor);
}

void GraphiItemCtrl::OnDrawTypeChanged( QAbstractButton * button )
{
    if(button == btn_Line)
    {
        i_CurrentType = ITM_TYPE_LINE;
    }
    else if (button == btn_Rectangle)
    {
        i_CurrentType = ITM_TYPE_RECTANGLE;
    }
    else if (button == btn_Elipse)
    {
        i_CurrentType = ITM_TYPE_ELIPSE;
    }
    else if(button == btn_Cross)
    {
        i_CurrentType = ITM_TYPE_CROSS;
    }
    else if(button == btn_Dot)
    {
        i_CurrentType = ITM_TYPE_DOT;
    }
    else if(button == btn_Circle)
    {
        i_CurrentType = ITM_TYPE_CIRCLE;
    }
}

void GraphiItemCtrl::OpenColorPallet()
{

    p_ColorDialog->open();
}

void GraphiItemCtrl::ColorSelected( QColor eColor )
{
    e_CurrentColor = eColor;
    o_CurrentPen.setColor(e_CurrentColor);

    QPalette oPlaet;
    oPlaet.setColor(QPalette::Background, e_CurrentColor);


    lbl_SelColor->setPalette(oPlaet);
}

void GraphiItemCtrl::OnLineWidth( int iWidth )
{
    i_CurrentWidth = iWidth;
    o_CurrentPen.setWidth(iWidth);
}

void GraphiItemCtrl::OnZoomChanged( QAbstractButton * button )
{
    if(button == btn_ZoomIn)
    {
        OnGraphicsScale(true);
    }
    else
    {
        OnGraphicsScale(false);
    }
}

void GraphiItemCtrl::OnMouseMoveEvent( QPointF oPoint )
{
    if(i_ActivityMode == ACTIVITY_MODE_RULER)
    {
        o_Ruler.setLine(o_PointA.x(),o_PointA.y(), oPoint.x(), oPoint.y());
        QPointF oTextPos;
        oTextPos.setX( (o_PointA.x() + oPoint.x()) /2);
        oTextPos.setY( (o_PointA.y() + oPoint.y()) /2);
        o_Text.setPos(oTextPos);

        double dX = fabs(o_PointA.x() - oPoint.x());
        double dY = fabs(o_PointA.y() - oPoint.y());
        double dLength = sqrt( (dX * dX) + (dY * dY) );
        dLength*=spin_ScaleFactor->value();
        o_Text.setPlainText(QString::number(dLength) + " " + txt_Suffix->text());

    }

    QString sValue;
    sValue.sprintf("%f x %f", oPoint.x(),oPoint.y());
    lbl_Coordinates->setText(sValue);



}

void GraphiItemCtrl::OnZoomValueChanged( double dScale)
{
    QTransform oTransform = QTransform::fromScale(dScale, dScale);
    p_GraphicView->setTransform(oTransform);
}

void GraphiItemCtrl::AddImage( QPixmap oImage , int iIndex ,QString sName/* = "Img"*/)
{
    if(b_InAutoMode)
    {
       // QMessageBox::warning(this,"Abort","Can not add images while auto");
       // return;
    }

    if(oImage.width() > i_MaxWidth)
        i_MaxWidth = oImage.width();

    if(oImage.height() > i_MaxHeight)
        i_MaxHeight = oImage.height();

    if(iIndex == 0)
    {
        iIndex = i_NextZIndex++;
    }
    int iResult = RemoveImage(iIndex, true);
    GraphicImage* pImage = new GraphicImage(oImage, this);
    pImage->s_Name = sName;
    set_Images.insert(pImage);
    map_ZIndices.insert(iIndex, pImage);

    if(iResult == ERR_SELECTED_IMAGE)
    {
        set_SelectedItems.insert(pImage);
    }
    pImage->setZValue(iIndex);

    qDebug() << "Z Index of image is" <<pImage->zValue() ;

    p_ItemList->AddItem(pImage, iIndex);

    o_ItemScene.addItem(pImage);

    //p_GraphicView->setMaximumHeight(i_MaxHeight);
    //p_GraphicView->setMaximumWidth(i_MaxWidth);
}

void GraphiItemCtrl::ClearAllImages()
{
    foreach(QGraphicsItem* pItem, set_Images)
    {
        delete pItem;
    }
    set_Images.clear();
    map_ZIndices.clear();
    p_ItemList->RemoveAll();
}

void GraphiItemCtrl::ClearAllDrawnItems()
{
    map_DrawnItems.clear();
    i_MaxIndex = 0;
    QList<QGraphicsItem*> lstItems = o_ItemScene.items();
    foreach(QGraphicsItem* pItem, lstItems)
    {
        if(pItem == &o_Line || pItem == &o_Rect || pItem == &o_Elipse)
            continue;

        if(!set_Images.contains(pItem))
        {
            o_ItemScene.removeItem(pItem);
            delete pItem;
        }
    }


}

void GraphiItemCtrl::ClearAllSelectionCheck()
{
    if(b_SingleDraw)
    {
        ClearAllDrawnItems();
    }
}

void GraphiItemCtrl::ReplaceFromIndex(int iIndex, QGraphicsItem* pReplaceWith)
{
    if(iIndex > i_MaxIndex)
    {
        i_MaxIndex = iIndex;
    }
    if(map_DrawnItems.contains(iIndex))
    {
        QGraphicsItem* pItemToDelete = map_DrawnItems.value(iIndex);
        delete pItemToDelete;
    }
    pReplaceWith->setData(DRAWITM_INDEX, iIndex);
    map_DrawnItems.insert(iIndex, pReplaceWith);
}

void GraphiItemCtrl::AddAutoIndex(QGraphicsItem* pLine)
{
    pLine->setData(DRAWITM_INDEX, ++i_MaxIndex);
    map_DrawnItems.insert(i_MaxIndex , pLine);
}

int GraphiItemCtrl::AddLine( QLineF oLine )
{
    ClearAllSelectionCheck();
    QGraphicsLineItem* pLine = o_ItemScene.addLine(oLine,o_CurrentPen);
    pLine->setZValue(Z_INDEX_DRAWN_ITEMS);

    AddAutoIndex(pLine);

    return i_MaxIndex;
}

void GraphiItemCtrl::AddLine(int iIndex, QLineF oLine)
{
    ClearAllSelectionCheck();
    QGraphicsLineItem* pLine = o_ItemScene.addLine(oLine,o_CurrentPen);
    pLine->setZValue(Z_INDEX_DRAWN_ITEMS);
    ReplaceFromIndex(iIndex, pLine);
}

int GraphiItemCtrl::AddRect( QRectF oRect )
{
    ClearAllSelectionCheck();
    QGraphicsRectItem* pRect = o_ItemScene.addRect(oRect,o_CurrentPen);
    pRect->setZValue(Z_INDEX_DRAWN_ITEMS);
    AddAutoIndex(pRect);
    return i_MaxIndex;
}

void GraphiItemCtrl::AddRect(int iIndex, QRectF oRect)
{
    ClearAllSelectionCheck();
    QGraphicsRectItem* pRect = o_ItemScene.addRect(oRect,o_CurrentPen);
    pRect->setZValue(Z_INDEX_DRAWN_ITEMS);
    ReplaceFromIndex(iIndex, pRect);
}

int GraphiItemCtrl::AddElipse( QRectF oRect )
{
    ClearAllSelectionCheck();
    QGraphicsEllipseItem* pElipse =	o_ItemScene.addEllipse(oRect,o_CurrentPen);
    pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);
    AddAutoIndex(pElipse);
    return i_MaxIndex;
}

void GraphiItemCtrl::AddElipse(int iIndex, QRectF oRect)
{
    ClearAllSelectionCheck();
    QGraphicsEllipseItem* pElipse =	o_ItemScene.addEllipse(oRect,o_CurrentPen);
    pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);
    ReplaceFromIndex(iIndex, pElipse);
}

QGraphicsEllipseItem * GraphiItemCtrl::DrawCircle(double dRadius, QPointF oCenter)
{
    double dX0 = oCenter.x() - dRadius;
    double dY0 = oCenter.y() - dRadius;
    double dWith = 2 * dRadius;
    QRectF oRect(dX0, dY0, dWith, dWith);
    ClearAllSelectionCheck();
    QGraphicsEllipseItem* pElipse =	o_ItemScene.addEllipse(oRect,o_CurrentPen);
    pElipse->setData(IS_CICLE, true);
    pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);

    return pElipse;
}

int GraphiItemCtrl::CopyAndGetIndex(QGraphicsItem *pSource, QGraphicsItem *pDestination)
{
    int iIndex = pSource->data(DRAWITM_INDEX).toInt();
    pDestination->setData(DRAWITM_INDEX, iIndex);
    return iIndex;
}

int GraphiItemCtrl::AddCircle(QPointF oCenter, double dRadius)
{
    QGraphicsEllipseItem* pElipse = DrawCircle(dRadius, oCenter);

    AddAutoIndex(pElipse);
    return i_MaxIndex;
}

void GraphiItemCtrl::AddCircle(int iIndex, QPointF oCenter, double dRadius)
{
     QGraphicsEllipseItem* pElipse = DrawCircle(dRadius, oCenter);
     ReplaceFromIndex(iIndex, pElipse);
}

int GraphiItemCtrl::RemoveImage( int iIndex, bool bReplace/* = false*/ )
{
    int iErrorCode = ERR_NONE;
    if(b_InAutoMode)
    {
        //QMessageBox::warning(this,"Abort","Can not remove images while auto");
        //return;
    }

    QMap<int, QGraphicsItem*>::Iterator itr = map_ZIndices.find(iIndex);
    if(itr== map_ZIndices.end())
        return iErrorCode;

    QGraphicsItem* pItem = *itr;
    GraphicImage* pGraphiImage = dynamic_cast<GraphicImage*>(pItem);
    if(pGraphiImage)
    {
        if(set_SelectedItems.contains(pGraphiImage))
        {
            //QMessageBox::warning(this,"Abort","Can not remove selected item while auto");
            //return;
            set_SelectedItems.remove(pGraphiImage);
            itr_Timer = set_SelectedItems.begin();

            iErrorCode = ERR_SELECTED_IMAGE;
        }
    }
    set_Images.remove(pItem);
    map_ZIndices.erase(itr);
    delete pItem;
    if(!bReplace)
    {
        p_ItemList->RemoveItem(iIndex);
    }
    return iErrorCode;
}

int GraphiItemCtrl::GetImageCount()
{
    return set_Images.size();
}

QGraphicsItem* GraphiItemCtrl::GetImage( int iIndex )
{
    QMap<int, QGraphicsItem*>::Iterator itr = map_ZIndices.find(iIndex);
    if(itr== map_ZIndices.end())
        return NULL;

    return *itr;
}

void GraphiItemCtrl::OnResetLastImage( GraphicImage* pImage )
{
    if(!pImage)
        return;
    int iValue = slr_ImageSlider->value();
    delete slr_ImageSlider;
    slr_ImageSlider = NULL;

    slr_ImageSlider = new QSlider(this);
    slr_ImageSlider->setObjectName(QStringLiteral("slr_ImageSlider"));

    slr_ImageSlider->setValue(iValue);
    slr_ImageSlider->setOrientation(Qt::Vertical);

    horizontalLayout->addWidget(slr_ImageSlider);
    pImage->SetSlier(slr_ImageSlider);

}

void GraphiItemCtrl::AddCross( QPointF oCenter )
{
    QPoint oP1,oP2,oP3,oP4;
    Util::GetCrossPoints(oCenter, oP1,oP2,oP3,oP4);

    QGraphicsLineItem* pLine = new QGraphicsLineItem(oP1.x(),
        oP1.y(),oP2.x(),oP2.y());
    pLine->setPen(o_CurrentPen);
    pLine->setZValue(Z_INDEX_DRAWN_ITEMS);
    QGraphicsLineItem* pLine2 = new QGraphicsLineItem(oP3.x(),
        oP3.y(),oP4.x(),oP4.y());
    pLine2->setPen(o_CurrentPen);
    pLine2->setZValue(Z_INDEX_DRAWN_ITEMS);

    QGraphicsItemGroup* pGroup = new QGraphicsItemGroup();
    pGroup->addToGroup(pLine);
    pGroup->addToGroup(pLine2);
    pGroup->setZValue(Z_INDEX_DRAWN_ITEMS);

    o_ItemScene.addItem(pGroup);


}

void GraphiItemCtrl::AddDot( QPointF oCenter )
{
    QBrush oBrush(o_CurrentPen.color());
    QGraphicsEllipseItem* pElipse = o_ItemScene.addEllipse(
        oCenter.x(),oCenter.y(),DOT_LEN, DOT_LEN, o_CurrentPen,oBrush);
    pElipse->setZValue(Z_INDEX_DRAWN_ITEMS);
}

void GraphiItemCtrl::GetDrawnElipseItems(std::vector<QRectF> &vecElipse)
{

    ElipseProvider ep(false);
    GetDrawnItems<QRectF, ElipseProvider>(ITM_TYPE_ELIPSE, ep, vecElipse);
}

void GraphiItemCtrl::GetDrawnCircleItems(std::vector<QRectF> &vecCircle)
{
    ElipseProvider cp(true);
    GetDrawnItems<QRectF, ElipseProvider>(ITM_TYPE_ELIPSE, cp, vecCircle);
}

void GraphiItemCtrl::GetDrawnLineItems(std::vector<QLineF> &vecLines)
{
    LineProvider lp;
    GetDrawnItems<QLineF, LineProvider>(ITM_TYPE_LINE, lp, vecLines);
}

void GraphiItemCtrl::GetDrawnRectItems(std::vector<QRectF> &vecRects)
{
    RectProvider rp;
    GetDrawnItems<QRectF, RectProvider>(ITM_TYPE_RECTANGLE, rp, vecRects);
}

void GraphiItemCtrl::OnAuto(int iOn)
{
    SetAllImgVisibility(false);
    set_SelectedItems = p_ItemList->GetSelectedImages();
    if(iOn == 0)
    {
        o_Timer.stop();
        p_ItemList->ResetAndLock(false);
        p_ItemList->OnSelectionChanged();
        b_InAutoMode = false;
        return;
    }
    b_InAutoMode = true;
    p_ItemList->ResetAndLock(true);
    itr_Timer = set_SelectedItems.begin();
    OnIterateTimer();
    int iStartValue = slr_Auto->value();
    o_Timer.start(iStartValue);

}

void GraphiItemCtrl::OnIterateTimer()
{
    itr_Timer++;
    if(itr_Timer == set_SelectedItems.end())
    {
        itr_Timer = set_SelectedItems.begin();
    }
    if(p_LastItem)
        p_LastItem->setVisible(false);

    QGraphicsItem* pItem = *itr_Timer;
    pItem->setVisible(true);
    p_LastItem = pItem;
}

void GraphiItemCtrl::OnImageSwitchTimeChange( int iValue )
{
    if(o_Timer.isActive())
        o_Timer.start(iValue);
}


void GraphiItemCtrl::OnRelocateChecked(QPointF oNewPoint)
{
    if(!p_SelectedItem)
    {
        qDebug() << "error";
        return;
    }
    int iType = p_SelectedItem->type();
    if(iType == ITM_TYPE_ELIPSE && p_SelectedItem->data(IS_CICLE).toBool())
    {
        iType = ITM_TYPE_CIRCLE;
    }
    switch(iType)
    {
        case ITM_TYPE_RECTANGLE:
        {
            QGraphicsRectItem* pRect = dynamic_cast<QGraphicsRectItem*>(p_SelectedItem);
            QRectF oCurRect = pRect->sceneBoundingRect();

            double dWidth = oNewPoint.x() - oCurRect.x();
            double dHeight = oNewPoint.y() - oCurRect.y();
            QPointF oPointA = oCurRect.topLeft();
            AdjustSizes(dHeight,oPointA,dWidth);
            QGraphicsRectItem* pRecti = o_ItemScene.addRect(oPointA.x(),oPointA.y(),
                dWidth ,dHeight,o_CurrentPen);
            pRecti->setZValue(Z_INDEX_DRAWN_ITEMS);
            int iIndex = CopyAndGetIndex(pRect, pRecti);
            o_ItemScene.removeItem(p_SelectionDot);
            delete p_SelectionDot;
            p_SelectionDot = NULL;

            o_ItemScene.removeItem(p_SelectedItem);
            delete p_SelectedItem;
            p_SelectedItem = NULL;
            //Resizing notification location
            //NotifyRectMoved(oCurRect, pRecti->sceneBoundingRect());
            NotifyItemResized(oCurRect, pRecti->sceneBoundingRect());
            NotifyRectResized(oCurRect, pRecti->sceneBoundingRect());

            NotifyRectResized(iIndex, oCurRect,  pRecti->sceneBoundingRect());
            qDebug() << "Notify Rect Resized "<< iIndex<< " : "<< oCurRect << "==" << pRecti->sceneBoundingRect();
            break;
        }
        case ITM_TYPE_ELIPSE:
        {
            QGraphicsEllipseItem* pElipse = dynamic_cast<QGraphicsEllipseItem*>(p_SelectedItem);
            QRectF oCurRect = pElipse->sceneBoundingRect();

            double dWidth = oNewPoint.x() - oCurRect.x();
            double dHeight = oNewPoint.y() - oCurRect.y();

            QPointF oPointA = oCurRect.topLeft();
            AdjustSizes(dHeight,oPointA,dWidth);

            QGraphicsEllipseItem* pItem = o_ItemScene.addEllipse(oPointA.x(),oPointA.y(),dWidth,dHeight,o_CurrentPen);
            pItem->setZValue(Z_INDEX_DRAWN_ITEMS);
            int iIndex = CopyAndGetIndex(pElipse, pItem);
            o_ItemScene.removeItem(p_SelectionDot);
            delete p_SelectionDot;
            p_SelectionDot = NULL;
            o_ItemScene.removeItem(p_SelectedItem);
            delete p_SelectedItem;
            p_SelectedItem = NULL;
            //NotifyElipseMoved(oCurRect, pItem->sceneBoundingRect());
            NotifyItemResized(oCurRect, pItem->sceneBoundingRect());
            NotifyElipseResized(oCurRect, pItem->sceneBoundingRect());

            NotifyElipseResized(iIndex, oCurRect, pItem->sceneBoundingRect());
            qDebug() << "Notify Elipse Resized "<< iIndex<< " : "<< oCurRect << "==" << pItem->sceneBoundingRect();
            break;
        }
    case ITM_TYPE_CIRCLE:
    {
        QGraphicsEllipseItem* pElipse = dynamic_cast<QGraphicsEllipseItem*>(p_SelectedItem);
        QRectF oCurRect = pElipse->sceneBoundingRect();

        float fTopLeftX, fTopLeftY, fWidth, fHeight;
        GetLeftAndWidth(o_PointA.x(), o_PointA.y(), oNewPoint.x(), oNewPoint.y(),fTopLeftX,
                        fTopLeftY, fWidth, fHeight);

        QGraphicsEllipseItem* pItem = o_ItemScene.addEllipse(fTopLeftX ,fTopLeftY,fWidth,fHeight,o_CurrentPen);
        pItem->setZValue(Z_INDEX_DRAWN_ITEMS);
        int iIndex = CopyAndGetIndex(pElipse, pItem);
        o_ItemScene.removeItem(p_SelectionDot);
        delete p_SelectionDot;
        p_SelectionDot = NULL;
        o_ItemScene.removeItem(p_SelectedItem);
        delete p_SelectedItem;
        p_SelectedItem = NULL;
        //NotifyElipseMoved(oCurRect, pItem->sceneBoundingRect());
        NotifyItemResized(oCurRect, pItem->sceneBoundingRect());
        NotifyCircleResized(oCurRect, pItem->sceneBoundingRect());


        NotifyCircleResized(iIndex, oCurRect, pItem->sceneBoundingRect());
        qDebug() << "Notify Circle Resized "<< iIndex<< " : "<< oCurRect << "==" << pItem->sceneBoundingRect();
        break;

    }
        case ITM_TYPE_LINE:
        {
            QGraphicsLineItem* pLineItem = dynamic_cast<QGraphicsLineItem*>(p_SelectedItem);
            QRectF oCurRect = pLineItem->sceneBoundingRect();

            QGraphicsLineItem* pItem = o_ItemScene.addLine(oCurRect.x(),oCurRect.y(),oNewPoint.x(),oNewPoint.y(),o_CurrentPen);
            pItem->setZValue(Z_INDEX_DRAWN_ITEMS);
            int iIndex = CopyAndGetIndex(pLineItem, pItem);
            o_ItemScene.removeItem(p_SelectionDot);
            delete p_SelectionDot;
            p_SelectionDot = NULL;
            o_ItemScene.removeItem(p_SelectedItem);
            delete p_SelectedItem;
            p_SelectedItem = NULL;

            NotifyLineResized(pLineItem->line(), pItem->line());

            NotifyLineResized(iIndex, pLineItem->line(), pItem->line());
            qDebug() << "Notify Line Resized "<< iIndex<< " : "<< pItem->line() << "==" << pLineItem->line();
            break;
        }
    }
}


void GraphiItemCtrl::SetAllImgVisibility( bool bVisibility )
{
    for(QSet<QGraphicsItem*>::Iterator itr = set_Images.begin(); itr != set_Images.end(); itr++)
    {
        QGraphicsItem* pImage = *itr;
        pImage->setVisible(bVisibility);
    }
}

void GraphiItemCtrl::OnClickMoveEvent( QPointF oPoint )
{
    if(i_ActivityMode == ACTIVTITY_MODE_DRAW)
    {
        switch(i_CurrentType)
        {
        case ITM_TYPE_LINE:
            {
                o_Line.setLine(o_PointA.x(),o_PointA.y(),oPoint.x(),oPoint.y() );
                break;
            }
        case ITM_TYPE_RECTANGLE:
            {
                double dWidth = (oPoint.x() - o_PointA.x());
                double dHeight = (oPoint.y() - o_PointA.y());
                QPointF oPointA = o_PointA;
                AdjustSizes(dHeight, oPointA, dWidth);

                o_Rect.setRect(oPointA.x(),oPointA.y(),dWidth,dHeight );
                break;
            }
        case ITM_TYPE_ELIPSE:
            {
                double dWidth = (oPoint.x() - o_PointA.x());
                double dHeight = (oPoint.y() - o_PointA.y());
                QPointF oPointA = o_PointA;
                AdjustSizes(dHeight, oPointA, dWidth);
                o_Elipse.setRect(oPointA.x(),oPointA.y(),dWidth,dHeight );
                break;
            }
        case ITM_TYPE_CIRCLE:
        {
            float fTopLeftX, fTopLeftY, fWidth, fHeight;
            GetLeftAndWidth(o_PointA.x(), o_PointA.y(), oPoint.x(), oPoint.y(),fTopLeftX,
                            fTopLeftY, fWidth, fHeight);

            QPointF oPointA = o_PointA;

            o_Elipse.setRect(fTopLeftX,fTopLeftY,fWidth,fHeight );
            break;
        }
        }
    }
}

void GraphiItemCtrl::OnMouseDoubleClickEvent(QPointF oPoint)
{
    QGraphicsItem* pItem = o_ItemScene.itemAt(oPoint,QTransform());
    if(pItem)
    {
        if(pItem != p_SelectionDot)
        {
            DeselectResize();
        }
       pItem->setFlag(QGraphicsItem::ItemIsMovable,false);
        switch(pItem->type())
        {
            case ITM_TYPE_RECTANGLE:
            {
                QGraphicsRectItem* pRect= dynamic_cast<QGraphicsRectItem*>(pItem);
                p_SelectedItem = pRect;
                QPointF oOpint = pRect->sceneBoundingRect().bottomRight();
                GraphicsResizeTracker* pTracker = new GraphicsResizeTracker(oOpint,this);
                o_ItemScene.addItem(pTracker);
                p_SelectionDot = pTracker;
                p_SelectionDot->setFlag(QGraphicsItem::ItemIsMovable);
                p_SelectionDot->setZValue(Z_INDEX_DRAWN_ITEMS+1);


                break;
            }
            case ITM_TYPE_ELIPSE:
        case ITM_TYPE_CIRCLE:
            {
                QGraphicsEllipseItem* pElipse = dynamic_cast<QGraphicsEllipseItem*>(pItem);
                p_SelectedItem = pElipse;
                QPointF oOpint = pElipse->sceneBoundingRect().bottomRight();
                GraphicsResizeTracker* pTracker = new GraphicsResizeTracker(oOpint,this);
                o_ItemScene.addItem(pTracker);
                p_SelectionDot = pTracker;
                p_SelectionDot->setFlag(QGraphicsItem::ItemIsMovable);
                p_SelectionDot->setZValue(Z_INDEX_DRAWN_ITEMS+1);
                break;
            }
            case ITM_TYPE_LINE:
            {
                QGraphicsLineItem* pLine = dynamic_cast<QGraphicsLineItem*>(pItem);
                p_SelectedItem = pLine;
                QPointF oOpint = pLine->sceneBoundingRect().bottomRight();
                GraphicsResizeTracker* pTracker = new GraphicsResizeTracker(oOpint,this);
                o_ItemScene.addItem(pTracker);
                p_SelectionDot = pTracker;
                p_SelectionDot->setFlag(QGraphicsItem::ItemIsMovable);
                p_SelectionDot->setZValue(Z_INDEX_DRAWN_ITEMS+1);
                break;
            }
            default:
            {
                DeselectResize();
            }
        }

    }
    else
    {
        if(p_SelectedItem)
        {
            p_SelectedItem = NULL;
            o_ItemScene.removeItem(p_SelectionDot);
            delete p_SelectionDot;
            p_SelectionDot = NULL;
        }
    }
    return;
}

void GraphiItemCtrl::AdjustSizes( double &dHeight, QPointF &oPointA, double &dWidth )
{
    if(dHeight < 0)
    {
        dHeight = fabs(dHeight);
        oPointA.setY(oPointA.y() - dHeight);
    }
    if(dWidth < 0)
    {
        dWidth = fabs(dWidth);
        oPointA.setX(oPointA.x() - dWidth);
    }
}






