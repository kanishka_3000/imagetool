#include "Util.h"


Util::Util(void)
{
}


Util::~Util(void)
{
}

void Util::GetCrossPoints( QPointF oCenter,QPoint& oP1, QPoint& oP2, 
	QPoint& oP3, QPoint& oP4 )
{
	oP1.setX(oCenter.x() - CROSS_LEN);oP1.setY(oCenter.y() + CROSS_LEN);
	oP2.setX(oCenter.x() + CROSS_LEN);oP2.setY(oCenter.y() - CROSS_LEN);
	oP3.setX(oCenter.x() - CROSS_LEN);oP3.setY(oCenter.y() - CROSS_LEN);
	oP4.setX(oCenter.x() + CROSS_LEN);oP4.setY(oCenter.y() + CROSS_LEN);
}
