#ifndef Util_h__
#define Util_h__
#include <QPoint>
#define CROSS_LEN	10
#define DOT_LEN		10
class Util
{
public:
	Util(void);
	virtual ~Util(void);
	static void GetCrossPoints(QPointF oCenter,QPoint& oP1, QPoint& oP2,
		QPoint& oP3, QPoint& oP4);


};
#endif // Util_h__

