#ifndef GRAPHIITEMCTRL_H
#define GRAPHIITEMCTRL_H

#define ITM_LINE	"Line"
#define ITM_ELIPSE	"Elipse"
#define ITM_RECTANGLE	"Rectangle"

#define ITM_TYPE_LINE	6
#define ITM_TYPE_ELIPSE	4
#define ITM_TYPE_RECTANGLE	3
#define ITM_TYPE_CROSS		7
#define ITM_TYPE_DOT	8
#define ITM_TYPE_CIRCLE 9

#define ACTIVTITY_MODE_DRAW		1
#define ACTIVTITY_MODE_SELECT	2
#define ACTIVITY_MODE_POINT		3
#define ACTIVITY_MODE_RULER		4

#define CUSTDATA_KEY	1


#define Z_INDEX_DRAWN_ITEMS	1000
#define ERR_NONE 0
#define ERR_SELECTED_IMAGE 1
#define IS_CICLE 4784
#define DRAWITM_INDEX 4790

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsPixmapItem>
#include <GraphicImage.h>
#include <QButtonGroup>
#include <QTimer>
#include <QGraphicsTextItem>
#include "ui_GraphiItemCtrl.h"
void GetLeftAndWidth(float x0, float y0, float x2, float y2, float& fTopLeftX,
                     float& fTopLeftY, float& fWidth, float& fHeight);
class QColorDialog;
class GraphiItemCtrl :public QWidget, public Ui::GraphiItemCtrl
{
    Q_OBJECT

public:

    GraphiItemCtrl( QWidget *parent = 0);
    ~GraphiItemCtrl();

    virtual void paintEvent ( QPaintEvent * event );
    virtual void OnMouseClickEvent(QPointF oPoint);
    virtual void OnMouseReleaseEvent(QPointF oPoint);
    virtual void OnMouseMoveEvent(QPointF oPoint);
    virtual void OnClickMoveEvent(QPointF oPoint);
    //Trigger point for resize item CR
    virtual void OnMouseDoubleClickEvent(QPointF oPoint);
    void AdjustSizes( double &dHeight, QPointF &oPointA, double &dWidth );

    virtual void OnGraphicsScale(bool bZoomIn);
    void UpdateCoordinates(QString sMsg){lbl_Coordinates->setText(sMsg);};
    void OnRemoveEvent();
    void OnDrawEvent();

    void AddImage(QPixmap oImage, int iIndex ,QString sName = "Img");
    int RemoveImage(int iIndex, bool bReplace = false);

    int GetImageCount();
    QGraphicsItem* GetImage(int iIndex);
    void ClearAllImages();
    void ClearAllDrawnItems();
    QSet<GraphicImage*> set_SelectedItems;
    GraphicImage* p_Image;
    QGraphicsScene o_ItemScene;

    QPointF o_PointA;
    QPointF o_PointB;
    bool b_MouseClicked;
    QGraphicsLineItem o_Ruler;
    QGraphicsTextItem o_Text;
    QString s_CurrentItem;
    int i_CurrentType;
    QButtonGroup o_MainActivity;
    QButtonGroup o_DrawType;
    QButtonGroup o_Zooming;

    //Interactive GraphicsItems
    QGraphicsLineItem o_Line;
    QGraphicsRectItem o_Rect;
    QGraphicsEllipseItem o_Elipse;

    //Control exit resize mode
    void DeselectResize();
    void ClearAllSelectionCheck();
private:

    QGraphicsItemGroup o_Group;

    QColor e_CurrentColor;
    QPen	o_CurrentPen;
    int i_CurrentWidth;
    QColorDialog* p_ColorDialog;
    int i_ActivityMode;
    QRectF o_OrigianlRect;
    qreal d_Scale;
    QSet<QGraphicsItem*> set_Images;
    QMap<int, QGraphicsItem*> map_ZIndices;
    QGraphicsItem* p_LastItem;
    QHBoxLayout* p_SlierLayout ;
    QTimer o_Timer;
    int i_MaxWidth;
    int i_MaxHeight;
    int i_NextZIndex;
    bool b_InAutoMode;

    QSet<GraphicImage*>::Iterator itr_Timer;

    QGraphicsItem* p_SelectedItem;

    QGraphicsItem* p_SelectionDot;
    QMap<int, QGraphicsItem*> map_DrawnItems;
    bool b_ItemRedrawn;
    bool b_SingleDraw;
    int i_MaxIndex;
signals:
    //>>> Signals that will be emmited when user draws
    //	items using the mouse pointer
    void NotifyLineAdded(QLineF oLine);
    void NotifyLineAdded(int iIndex, QLineF oLine);
    void NotifyRectAdded(QRectF oRect);
    void NotifyRectAdded(int iIndex, QRectF oRect);
    void NotifyElipseAdded(QRectF oRect);
    void NotifyElipseAdded(int iIndex, QRectF oRect);
    void NotifyCircleAdded(QRectF oRect);
    void NotifyCircleAdded(int iIndex, QRectF oRect);

    //Notifies when any item is moved
    void NotifyItemMoved(QRectF oPrev, QRectF oNew);

    //Notify when individual item is moved after firing itemmoved
    void NotifyLineMoved(QLineF oPrev, QLineF oNew);
    void NotifyLineMoved(int iIndex, QLineF oPrev, QLineF oNew);
    void NotifyRectMoved(QRectF oPrev, QRectF oNew);
    void NotifyRectMoved(int iIndex, QRectF oPrev, QRectF oNew);
    void NotifyElipseMoved(QRectF oPrev, QRectF oNew);
    void NotifyElipseMoved(int iIndex, QRectF oPrev, QRectF oNew);
    void NotifyCircleMoved(QRectF oPrev, QRectF oNew);
    void NotifyCircleMoved(int iIndex, QRectF oPrev, QRectF oNew);
    //Notifies when any item is resized
    void NotifyItemResized(QRectF oPrev, QRectF oNew);

    //Notify when individual item is resized, after firing itemreseized
    void NotifyLineResized(QLineF oPrev, QLineF oNew);
    void NotifyLineResized(int iIndex, QLineF oPrev, QLineF oNew);
    void NotifyRectResized(QRectF oPrev, QRectF oNew);
    void NotifyRectResized(int iIndex, QRectF oPrev, QRectF oNew);
    void NotifyElipseResized(QRectF oPrev, QRectF oNew);
    void NotifyElipseResized(int iIndex, QRectF oPrev, QRectF oNew);
    void NotifyCircleResized(QRectF oPrev, QRectF oNew);
    void NotifyCircleResized(int iIndex, QRectF oPrev, QRectF oNew);
    //<<<

public slots:
    void OnDrawItemChanged(QString sItemType);
    void OnDrawTypeChanged(QAbstractButton * button);
    void OnButtonSelected(QAbstractButton * button);
    void OnZoomChanged(QAbstractButton * button);
    void OpenColorPallet();
    void ColorSelected(QColor eColor);
    void OnLineWidth(int iWidth);
    void OnZoomValueChanged(double dScale);
    void OnResetLastImage(GraphicImage* pImage);
    void OnAuto(int iOn);

    void SetAllImgVisibility( bool bVisibility );

    void OnIterateTimer();
    void OnImageSwitchTimeChange(int iValue);

    void OnRelocateChecked(QPointF oNewPoint);

    // >>>To add External Items

    int AddLine(QLineF oLine);
    void AddLine(int iIndex, QLineF oLine);
    int AddRect(QRectF oRect);
    void AddRect(int iIndex, QRectF oRect);
    int AddElipse(QRectF oRect);
    void AddElipse(int iIndex, QRectF oRect);
    int AddCircle(QPointF oCenter, double dRadius);
    void AddCircle(int iIndex, QPointF oCenter, double dRadius);
    void AddCross(QPointF oCenter);
    void AddDot(QPointF oCenter);
    //<<<
public:
    //If set true only one item will be drawn on the layout.
    //During second drawing all previous items will be erased.
    //Defaulted to false.
    void SetSingleItemMode(bool bSingleItem){b_SingleDraw = bSingleItem;}
    class ElipseProvider
    {
    public:
        ElipseProvider(bool bIsCircle)
        {
            b_IsCircle = bIsCircle;
        }
        QRectF operator()(QGraphicsItem* pItem, bool& bValid)
        {
            QGraphicsEllipseItem* pRect = dynamic_cast<QGraphicsEllipseItem*>(pItem);
            if(pRect->data(IS_CICLE).toBool() == b_IsCircle)
            {
                bValid = true;
                return pRect->rect();
            }
            else
            {
                bValid = false;
                QRectF oRec(0,0,0,0);
                return oRec;
            }
        }
    private:
        bool b_IsCircle;
    };
    class LineProvider
    {
    public:
        QLineF operator() (QGraphicsItem* pItem, bool& bValid)
        {
            bValid = true;
            QGraphicsLineItem* pLine = dynamic_cast<QGraphicsLineItem*>(pItem);
            return pLine->line();
        }
    };
    class RectProvider
    {
    public:
        QRectF operator() (QGraphicsItem* pItem, bool& bValid)
        {
            bValid = true;
            QGraphicsRectItem* pRectItem = dynamic_cast<QGraphicsRectItem*>(pItem);
            return pRectItem->rect();
        }
    };

    void GetDrawnElipseItems(std::vector<QRectF>& vecElipse);
    void GetDrawnCircleItems(std::vector<QRectF>& vecCircle);
    void GetDrawnLineItems(std::vector<QLineF>& vecLines);
    void GetDrawnRectItems(std::vector<QRectF>& vecRects);

protected:
    void ReplaceFromIndex(int iIndex, QGraphicsItem *pReplaceWith);

    void AddAutoIndex(QGraphicsItem *pLine);

    QGraphicsEllipseItem * DrawCircle(double dRadius, QPointF oCenter);
    int CopyAndGetIndex(QGraphicsItem* pSource, QGraphicsItem* pDestination);
private:
    template<class T, class F>
    void GetDrawnItems(int iType, F ftr,std::vector<T>& vecItems)
    {
        QList<QGraphicsItem*> lstItems = o_ItemScene.items();
        foreach(QGraphicsItem* pItem, lstItems)
        {
            int iTypei = pItem->type();
            if(iType == iTypei)
            {
                bool bValid;
                T t = ftr(pItem, bValid);
                if(!bValid)
                {
                    continue;
                }
                vecItems.push_back(t);
            }

        }
    }
};

#endif // GRAPHIITEMCTRL_H
