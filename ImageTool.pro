#-------------------------------------------------
#
# Project created by QtCreator 2015-08-26T21:25:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageTool
TEMPLATE = app


SOURCES += \
    Util.cpp \
    main.cpp \
    GraphiItemCtrl.cpp \
    GraphicsResizeTracker.cpp \
    GraphicsImageList.cpp \
    GraphicImage.cpp \
    GraphicHolderCtrl.cpp

HEADERS  += \
    Util.h \
    GraphiItemCtrl.h \
    GraphicsResizeTracker.h \
    GraphicsImageList.h \
    GraphicImage.h \
    GraphicHolderCtrl.h

RESOURCES += \
    ImageTool.qrc

FORMS += \
    GraphiItemCtrl.ui \
    GraphicsImageList.ui \
    GraphicHolderCtrl.ui

DISTFILES += \
    ImageTool.pro.user
