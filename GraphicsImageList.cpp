#include "GraphicsImageList.h"
#include <QIcon>
#include <QDebug>
#include <QMessageBox>
GraphicsImageList::GraphicsImageList(QWidget *parent)
    : QListWidget(parent),p_LastImage(NULL)
{
    setupUi(this);

    setDragEnabled(true);
    setDragDropMode(InternalMove);
    setSelectionMode(ExtendedSelection);
    /*connect(this, SIGNAL(itemSelectionChanged ()),
        this, SLOT(OnSelectionChanged()));*/
    connect(this, SIGNAL(itemClicked( QListWidgetItem *)),
        this, SLOT(OnItemClicked(QListWidgetItem* )));

}

GraphicsImageList::~GraphicsImageList()
{

}


void GraphicsImageList::AddItem( GraphicImage* pImage, int iIndex )
{
    if(iIndex > 0)
        iIndex--; // indices are 1 based in the application.

    int iZindex = iIndex;
    Qt::CheckState eCheckState = Qt::Unchecked;
    if(map_Indices.contains(iIndex))
    {
        QListWidgetItem* pItem = map_Indices[iIndex];
        iZindex = pItem->data(1000).toInt();//insert item to
                                            //the location of the original
        map_Images.remove(pItem);
        set_SelectedItems.remove(pItem);
        eCheckState = pItem->checkState();
        delete pItem;
    }

    QPixmap oImage = pImage->pixmap().scaled(16,16);
    QListWidgetItem* pItem = new QListWidgetItem(QIcon(oImage),pImage->s_Name);

    pItem->setFlags(pItem->flags() | Qt::ItemIsUserCheckable);
    pItem->setCheckState(eCheckState);
    pItem->setData(1000, iZindex);
    pImage->setZValue(iZindex);
    map_Images.insert(pItem, pImage);
    map_Indices.insert(iIndex, pItem);


    std::map<int,QListWidgetItem*> mapSort;
    mapSort.insert(std::pair<int, QListWidgetItem*>(iZindex,pItem));
    for(WidgetMap::Iterator itr = map_Indices.begin(); itr!=map_Indices.end(); itr++)
    {
        QListWidgetItem* ptItem = *itr;
        int itIndex = ptItem->data(1000).toInt();
        mapSort.insert(std::pair<int, QListWidgetItem*>(itIndex,ptItem));
    }
    int iIdx = 0;
    for(std::map<int,QListWidgetItem*>::reverse_iterator itr = mapSort.rbegin(); itr!= mapSort.rend(); itr++)
    {
        insertItem(iIdx,itr->second);
        iIdx++;
    }
    //insertItem(iZindex, pItem);

    OnItemClicked(pItem);
}


void GraphicsImageList::dropEvent( QDropEvent * pEvent )
{

    QListWidget::dropEvent(pEvent);
    int iRowCount = count();
    QListWidgetItem* pLastItem = NULL;
    for(int i = 0; i < iRowCount; i++)
    {
        QListWidgetItem* pItem = item(i);
        GraphicImage* pImage = map_Images[pItem];
        int iPrevIndex = pImage->zValue();
        int iRevIndex = iRowCount - i;
        pImage->setZValue(iRevIndex);
        pItem->setData(1000,iRevIndex);
        if(pItem->checkState() == Qt::Checked && pLastItem == NULL)
        {
            pLastItem = pItem;
        }
        qDebug() << "Index Re arrange" << iPrevIndex <<" - " <<i;
    }

    if(p_LastImage != pLastItem)
    {
        p_LastImage = pLastItem;
        GraphicImage* pImage = map_Images[p_LastImage];
        NotifySelectedLastIndex(pImage);
    }
}

void GraphicsImageList::OnSelectionChanged()
{

    GraphicImage* pLastImage = NULL;
    QListWidgetItem* pLastItem = NULL;
    for(WidgetMap::Iterator itr = map_Indices.begin();
        itr != map_Indices.end(); itr++)
    {

        QListWidgetItem* pItem = *itr;
        GraphicImage* pImage = map_Images[pItem];
        pItem->setBackgroundColor(Qt::white);

        if(pItem->checkState() == Qt::Checked)
        {
            pImage->setVisible(true);
            pLastImage = pImage;
            pLastItem = pItem;
        }
        else
        {
            pImage->setVisible(false);
        }

    }
    if(pLastItem)
    {
        pLastItem->setBackgroundColor(Qt::green);
    }
    NotifySelectedLastIndex(pLastImage);
}

void GraphicsImageList::OnItemClicked( QListWidgetItem* pItem )
{

    GraphicImage* pImage = map_Images[pItem];
    if(pItem->checkState() == Qt::Unchecked)
    {
        pImage->setVisible(false);
        set_SelectedItems.remove(pItem);

    }else if(set_SelectedItems.size() == 2)
    {
        pItem->setCheckState(Qt::Unchecked);
        pImage->setVisible(false);
        set_SelectedItems.remove(pItem);
        QMessageBox::information(this, "Too Many Items", "Please select only two items");
        return;
    }
    else
    {
        set_SelectedItems.insert(pItem);
        pImage->setVisible(true);
    }

    QListWidgetItem* pLastItem = NULL;
    for(QSet<QListWidgetItem*>::Iterator itr = set_SelectedItems.begin();
        itr != set_SelectedItems.end(); itr++)
    {
        QListWidgetItem* pItem = *itr;
        if(!pLastItem)
        {
            pLastItem = *itr;
            continue;
        }

        if(pLastItem->data(1000).toInt() < pItem->data(1000).toInt())
        {
            pLastItem = pItem;
        }
    }

    if(p_LastImage != pLastItem)
    {
        p_LastImage = pLastItem;
        GraphicImage* pImage = map_Images[p_LastImage];
        NotifySelectedLastIndex(pImage);
    }

}

void GraphicsImageList::RemoveItem( int iIndex )
{
    if(iIndex > 0)
        iIndex--; // indices are 1 based in the application.

    WidgetMap::Iterator itr = map_Indices.find(iIndex);
    if(itr == map_Indices.end())
        return;

    QListWidgetItem* pItem = *itr;
    map_Images.remove(pItem);
    set_SelectedItems.remove(pItem);
    map_Indices.erase(itr);

    delete pItem;
}

void GraphicsImageList::RemoveAll()
{
    for(WidgetMap::Iterator itr = map_Indices.begin();
        itr != map_Indices.end(); itr++)
    {
        QListWidgetItem* pItem = *itr;
        map_Images.remove(pItem);
        set_SelectedItems.remove(pItem);
        delete pItem;
    }
    map_Indices.clear();
}

void GraphicsImageList::ResetAndLock( bool bLock )
{

    setDisabled(bLock);

}

QSet<GraphicImage*> GraphicsImageList::GetSelectedImages()
{
    QSet<GraphicImage*> setSelecteItems;
    for(QSet<QListWidgetItem*>::Iterator itr = set_SelectedItems.begin();
        itr != set_SelectedItems.end(); itr++)
    {
            QListWidgetItem* pItem = *itr;
            GraphicImage* pImage = map_Images[pItem];
            setSelecteItems.insert(pImage);
    }
    return setSelecteItems;
}

