#include "GraphicHolderCtrl.h"
#include <GraphiItemCtrl.h>
#include <QFileDialog>
#include <QDebug>
GraphicHolderCtrl::GraphicHolderCtrl(QWidget *parent)
	: QWidget(parent)
{
	setupUi(this);
	//>>> Sample connections to demostrate object adding callbacks
	connect(p_LeftWidget, SIGNAL(NotifyLineAdded(QLineF)),
		this, SLOT(OnDemoLineAdded( QLineF  )));
	connect(p_LeftWidget, SIGNAL(NotifyRectAdded(QRectF )),
		this, SLOT(OnDemoRectAdded( QRectF  )));
	connect(p_LeftWidget, SIGNAL(NotifyElipseAdded(QRectF )),
		this, SLOT(OnDemoElipseAdded( QRectF  )));
    connect(p_LeftWidget, SIGNAL(NotifyCircleAdded(QRectF)),
            this, SLOT(OnDemoCircleAdded(QRectF)));
	//<<< Sample connections to demostrate object adding callbacks
}

GraphicHolderCtrl::~GraphicHolderCtrl()
{

}

void GraphicHolderCtrl::OpenFileDialog()
{
	s_CurrentFileName = QFileDialog::getOpenFileName(this,"AddImage");
}

void GraphicHolderCtrl::OnOk()
{
	if(s_CurrentFileName.isEmpty())
		return;

	GraphiItemCtrl* pCtrl = NULL;
	if(cmb_Type->currentText() == "Left")
	{
		pCtrl = p_LeftWidget;
	}
	else
	{
		pCtrl = p_RightWidget_2;
	}

	pCtrl->AddImage(QPixmap(s_CurrentFileName),spin_Index->value(),txt_Name->text());
	s_CurrentFileName = "";
}

void GraphicHolderCtrl::OnClearAll()
{
	GraphiItemCtrl* pCtrl = NULL;
	if(cmb_Type->currentText() == "Left")
	{
		pCtrl = p_LeftWidget;
	}
	else
	{
		pCtrl = p_RightWidget_2;
	}
	pCtrl->ClearAllImages();

}

void GraphicHolderCtrl::OnClearDrawings()
{
	GraphiItemCtrl* pCtrl = NULL;
	if(cmb_Type->currentText() == "Left")
	{
		pCtrl = p_LeftWidget;
	}
	else
	{
		pCtrl = p_RightWidget_2;
	}
	pCtrl->ClearAllDrawnItems();
}

void GraphicHolderCtrl::OnDemoLineAdded( QLineF oLine )
{
	qDebug() << oLine.p1().x();
}

void GraphicHolderCtrl::OnDemoRectAdded( QRectF oRect )
{
	qDebug() << oRect.x();
}

void GraphicHolderCtrl::OnDemoElipseAdded( QRectF oRect )
{
    qDebug() << "OnDemoElipseAdded" << oRect.x();
}

void GraphicHolderCtrl::OnDemoCircleAdded(QRectF oRect)
{
    qDebug()<< "OnDemoCircleAdded" << oRect.x();
}

void GraphicHolderCtrl::OnRemove()
{
	GraphiItemCtrl* pCtrl = NULL;
	if(cmb_Type->currentText() == "Left")
	{
		pCtrl = p_LeftWidget;
	}
	else
	{
		pCtrl = p_RightWidget_2;
	}

	pCtrl->RemoveImage(spin_Index->value());
}
