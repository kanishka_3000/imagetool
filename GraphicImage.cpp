#include "GraphicImage.h"
#include <GraphiItemCtrl.h>
#include <QDebug>
GraphicImage::GraphicImage(QPixmap oImage,GraphiItemCtrl* pParent)
    :QGraphicsPixmapItem(oImage),p_Parent(pParent)
{
    setAcceptHoverEvents(true);
    p_Slier = NULL;
    setVisible(false);
}


GraphicImage::~GraphicImage(void)
{
    //delete p_Slier;//parent is ImageCtrl, will be destroyed there
}

void GraphicImage::mousePressEvent( QGraphicsSceneMouseEvent * pEvent )
{


    p_Parent->OnMouseClickEvent(mapToScene(pEvent->pos()));
   // pEvent->accept();

}



void GraphicImage::mouseReleaseEvent( QGraphicsSceneMouseEvent * pEvent )
{

    p_Parent->OnMouseReleaseEvent(pEvent->pos());
    QGraphicsPixmapItem::mouseReleaseEvent(pEvent);
}

void GraphicImage::hoverMoveEvent( QGraphicsSceneHoverEvent * event )
{
    p_Parent->OnMouseMoveEvent(mapToScene(event->pos()));
    QGraphicsPixmapItem::hoverMoveEvent(event);
}

void GraphicImage::OnOpacityChanged( int iOpacity )
{
    double dOpacity = ((double)iOpacity/(double)10);
    setOpacity(dOpacity);
}

void GraphicImage::SetSlier( QSlider* pSlier )
{
    p_Slier = pSlier;
    pSlier->setMaximumSize(QSize(80, 15));
    pSlier->setMaximum(10);

    pSlier->setOrientation(Qt::Horizontal);
    pSlier->setTickPosition(QSlider::TicksBelow);
    connect(pSlier, SIGNAL(sliderMoved(int)), this ,SLOT(OnOpacityChanged(int)));
    OnOpacityChanged(p_Slier->value());
}

void GraphicImage::mouseMoveEvent( QGraphicsSceneMouseEvent * event )
{
    p_Parent->OnClickMoveEvent(mapToScene(event->pos() ));
    QGraphicsPixmapItem::mouseMoveEvent(event);
}

void GraphicImage::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    p_Parent->OnMouseDoubleClickEvent(mapToScene(event->pos()));
}


